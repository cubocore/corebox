/*
    *
    * This file is a part of CoreBox.
    * A Desktop for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QDebug>
#include <xcb/xcb.h>

#include "x11support.h"
#include "cxcbeventfilter.h"


bool CXCBEventFilter::nativeEventFilter(const QByteArray &eventType, void *message, long *)
{
    if (eventType != "xcb_generic_event_t")
        qDebug() << eventType;

    xcb_generic_event_t *ev = static_cast<xcb_generic_event_t *>(message);
    if (m_x11support) m_x11support->onX11Event(ev);
    return false;
}

void CXCBEventFilter::setX11Support(X11Support *x11support)
{
    m_x11support = x11support;
}
