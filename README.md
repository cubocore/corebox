# CoreBox
A Desktop from the CoreApps family.

# Merged with [CoreStuff](https://gitlab.com/cubocore/corestuff). So this repo is no longer active.

<img src="corebox.png" width="500">

### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/corebox/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt5
* qt5x11extras
* [libcprime](https://gitlab.com/cubocore/libcprime)
* [libcsys](https://gitlab.com/cubocore/libcsys)
* libxrender
* libxcomposite
* libx11
* libxdamage

### Information
Please see the Wiki page for this info.[Wiki page](https://gitlab.com/cubocore/wiki) .
* Changes in latest release
* Build from the source
* Tested In
* Known Bugs
* Help Us

### Feedback
* We need your feedback to improve the CoreBox.Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/-/issues).