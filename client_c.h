/*
    *
    * This file is a part of CoreBox.
    * A Desktop for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#ifndef CLIENT_C_H
#define CLIENT_C_H

#include <QObject>
#include <QIcon>
#include <QMap>


// Used for tracking connected windows (X11 clients).
// Client may have it's DockItem, but not necessary (for example, special windows are not shown in dock).
class ClientT
{

public:
    ClientT(unsigned long handle);
    ~ClientT();

    unsigned long handle() const{
        return m_handle;}

    bool isVisible(){
        return m_visible;}

    const QString& name() const{
        return m_name;}

    const QIcon& icon() const{
        return m_icon;}

    bool isUrgent() const{
        return m_isUrgent;}

    void windowPropertyChanged(unsigned long atom);

private:
    void updateVisibility();
    void updateName();
    void updateIcon();
    void updateUrgency();

    unsigned long m_handle;
    QString m_name;
    QIcon m_icon;
    bool m_isUrgent, m_visible;
};

#endif // CLIENT_C_H
