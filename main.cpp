/*
    *
    * This file is a part of CoreBox.
    * A Desktop for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include "corebox.h"

#include <QApplication>

#include <cprime/cprime.h>
#include <cprime/settingsmanage.h>

#include "button.h"


void startSetup()
{
    // set the requried folders
    CPrime::ValidityFunc::setupFileFolder( CPrime::FileFolderSetup::PinsFolder );
    CPrime::ValidityFunc::setupFileFolder( CPrime::FileFolderSetup::MimeFile );

    // if setting file not exist create one with defult
//    SettingsManage *sm = SettingsManage::instance();
//    sm->createDefaultSettings();
}

int main( int argc, char *argv[] )
{
	#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
		QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
	#endif

    QApplication app( argc, argv );
    app.setQuitOnLastWindowClosed(true);

    startSetup();

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName("CoreBox");

    DockButton s;
    s.show();

    return app.exec();
}
