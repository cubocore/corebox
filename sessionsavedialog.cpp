/*
    *
    * This file is a part of CoreBox.
    * A Desktop for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QSettings>
#include <QDir>
#include <QDebug>
#include <QDateTime>
#include <QTimer>
#include <QCompleter>
#include <QTableWidgetItem>
#include <QMessageBox>
#include <QInputDialog>
#include <QtConcurrent/QtConcurrent>

#include "sessionsavedialog.h"
#include "ui_sessionsavedialog.h"

#include <cprime/cprime.h>
#include <cprime/applicationdialog.h>
#include <cprime/desktopfile.h>


sessionSaveDialog::sessionSaveDialog(bool editAction, QString sessionName, /*QString date,*/ QWidget *parent) : QDialog(parent), ui(new Ui::sessionSaveDialog)
{
    ui->setupUi(this);
    m_action = editAction;

    // Get the Session List
    QSettings session(CPrime::Variables::CC_CoreApps_SessionFilePath(), QSettings::IniFormat);
    nameList = session.childGroups();

    if (m_action == 0) {
        ///////////////////////////////////////////////////
        ///                Add Session                  ///
        ///////////////////////////////////////////////////

        itemCount = 0;

        this->setWindowTitle("Add Session");
        ui->sessionName->textChanged("");
        ui->addAppPath->setVisible(0);
        ui->removeAppPath->setVisible(0);
        ui->pathEdit->setVisible(0);
        ui->ok->setEnabled(0);

    } else if (m_action == 1) {
        ///////////////////////////////////////////////////
        ///                Edit Session                 ///
        ///////////////////////////////////////////////////

        // Session is two layered groups
        // first   session name
        // second  appname
        // key     date time
        // value   path

        itemCount = 0;

        this->setWindowTitle("Edit Session");
        ui->sessionName->setText(sessionName);// Selected session name will be here

        ui->sessionName->setEnabled(0);
        ui->removeAppPath->setVisible(0);
        ui->pathEdit->setVisible(0);

        QTableWidgetItem *items;

        // begin session name group
        session.beginGroup(sessionName);

        // contains app name groups
        QStringList appNames = session.childGroups();

        foreach (QString appName, appNames) {
            // Begin app name group
            session.beginGroup(appName);

            // Date time keys
            QStringList dateTimes = session.childKeys();

            foreach (QString dateTime, dateTimes) {
                int index = ui->sesList->rowCount();
                ui->sesList->setRowCount(index + 1);
                // Set the app name
                items = new QTableWidgetItem(CPrime::ThemeFunc::getAppIcon(appName), appName);

                ui->sesList->setItem(index, 0, items);

                // Set the path
                QString path = session.value(dateTime).toString();
                items = new QTableWidgetItem(path);
                items->setData(Qt::UserRole, appName + "\t\t\t" + path + "\t\t\t" + dateTime);

                ui->sesList->setItem(index, 1, items);
            }

            // end date time group
            session.endGroup();
        }

        // end session name group
        session.endGroup();
    }

    ui->done->setVisible(false);
    connect(ui->cancel, &QToolButton::clicked, this, &sessionSaveDialog::close);
    connect(ui->path, &QLineEdit::textChanged, this, &sessionSaveDialog::enableDone);
    connect(ui->app, &QLineEdit::textChanged, this, &sessionSaveDialog::enableDone);
    connect(this, &sessionSaveDialog::enableOkSignal, this, &sessionSaveDialog::enableOk);
}

sessionSaveDialog::~sessionSaveDialog()
{
    delete ui;
}

void sessionSaveDialog::on_ok_clicked()
{
    bool itemAdded = false;
    QString sName = ui->sessionName->text();

    if (!sName.count()) {
        CPrime::InfoFunc::messageEngine("Session Name empty\nNot Saved", CPrime::MessageType::Info, this);
        return;
    }

    if (sName.count() && ui->sesList->rowCount()) {
        QSettings session(CPrime::Variables::CC_CoreApps_SessionFilePath(), QSettings::IniFormat);

        if (session.childGroups().count() > 15) {
            CPrime::InfoFunc::messageEngine("Session save limit reached", CPrime::MessageType::Warning, this);
            return;
        }

        // Begin Session Name group
        session.beginGroup(sName);

        for (int i = 0; i < ui->sesList->rowCount(); i++) {

            if (!ui->sesList->item(i, 0)) {
                qDebug() << "Error : Empty item.";
                continue;
            }

            // App name key
            QString appName = ui->sesList->item(i, 0)->text();
            // Path value
            QString path = ui->sesList->item(i, 1)->text();

            if (!appName.count() && !path.count()) {
                qDebug() << "Error : Everything empty.";
                continue;
            }

            // Date time group
            QString dtGroup = ui->sesList->item(i, 0)->data(Qt::UserRole).toString();

            if (dtGroup.count()) {
                QStringList temp = ui->sesList->item(i, 1)->data(Qt::UserRole).toString().split("\t\t\t");
                QString oAppName;
                QString oPath;
                if (temp.count() == 3) {
                    oAppName = temp.at(0);
                    oPath    = temp.at(1);
                }
                qDebug() << oAppName << oPath << appName << path;
                if (m_action == 1) {
                    if (oAppName == appName || oPath == path) {
                        qDebug() << "Old app or path equal : Deleting app and adding new app.";
                        session.remove(oAppName);
                    } else if (oAppName.count() && oAppName != appName && oPath != path) {
                        qDebug() << "Old app or path not equal : Deleting app and adding new app.";
                        session.remove(oAppName);
                    }
                }

                // Begin date time group
                session.beginGroup(appName);

                session.setValue(dtGroup, path);
                // End date time group
                session.endGroup();
                if (session.status() == QSettings::NoError) {
                    itemAdded = true;
                }
            } else {
                qDebug() << "Warning : This app already been added.";
            }
        }

        // End Session name group
        session.endGroup();

        if (itemAdded) {
            CPrime::InfoFunc::messageEngine("Session Changed", CPrime::MessageType::Info, this);
        }

        accept();        
    } else {
        qDebug() << "Warning : " << ui->sesList->rowCount();
    }
}

void sessionSaveDialog::on_sessionName_textChanged(const QString &arg1)
{
    if (m_action == 0) {
        if (arg1.count()) {
            if (!nameList.contains(arg1)) {
                ui->addAppPath->setVisible(1);
                ui->cMessage->setText("");
            } else {
                ui->addAppPath->setVisible(0);
                ui->ok->setEnabled(0);
                ui->cMessage->setText("<p style=color:red>Session name exits.</p>");
            }
        } else {
            ui->addAppPath->setVisible(0);
            ui->ok->setEnabled(0);
            ui->cMessage->setText("<p style=color:red>Enter Session name.</p>");
        }
    }
}

void sessionSaveDialog::on_changeApp_clicked()
{
    // Select application in the dialog
    ApplicationDialog *dialog = new ApplicationDialog(this);
    //dialog->getApplications();
    if (dialog->exec()) {
      if (dialog->getCurrentLauncher().compare("") != 0) {
        ui->app->setText(dialog->getCurrentLauncher());
      }
    }
}

void sessionSaveDialog::on_changePath_clicked()
{
    // ask if it is a file or folder with a messagebox and create a file or folder dialog
    QString msg = QString("Choose Add File for Selecting file and\nAdd Folder for selecting folder...");
    QMessageBox message(QMessageBox::Question, tr("Select Open Mode"), msg, QMessageBox::Cancel/*QMessageBox::Yes | QMessageBox::No*/);
    QAbstractButton *pButtonUrl = message.addButton("Add URL", QMessageBox::YesRole);
    QAbstractButton *pButtonYes = message.addButton("Add File", QMessageBox::YesRole);
    QAbstractButton *pButtonNo  = message.addButton("Add Folder", QMessageBox::YesRole);
    message.setWindowIcon(QIcon(":/icons/corestuff.svg"));

    int merge = message.exec();

    if (merge == QMessageBox::Cancel){
        return;
    }

    if (message.clickedButton() == pButtonYes) {
        QString filePath = QFileDialog::getOpenFileName(this, "Select File", QDir::homePath());
        if (filePath.count()) {
            ui->path->setText(filePath);
        }
    } else if (message.clickedButton() == pButtonNo) {
        QString folderPath = QFileDialog::getExistingDirectory(this, "Select Folder", QDir::homePath());
        if (folderPath.count()) {
            ui->path->setText(folderPath);
        }
    } else if (message.clickedButton() == pButtonUrl) {

        QDialog *widUrl = new QDialog(this);
        widUrl->setWindowTitle("Add URL");
        widUrl->resize(280, 120);

        QLineEdit *txtUrl   = new QLineEdit(widUrl);
        txtUrl->setPlaceholderText("Add URL here with 'http://'...");

        QLabel *lblUrl      = new QLabel(widUrl); // For showing validation

        QPushButton *btnOk  = new QPushButton(widUrl);
        btnOk->setText("OK");
        btnOk->setEnabled(false);

        QPushButton *btnCnl = new QPushButton(widUrl);
        btnCnl->setText("Cancel");

        QHBoxLayout *btnS = new QHBoxLayout;
        btnS->addWidget(btnOk);
        btnS->addWidget(btnCnl);

        QVBoxLayout *mainLayout = new QVBoxLayout;
        mainLayout->addWidget(txtUrl);
        mainLayout->addWidget(lblUrl);
        mainLayout->addLayout(btnS);

        widUrl->setLayout(mainLayout);

        connect(btnCnl, &QPushButton::clicked, widUrl, [&, widUrl]() {
            widUrl->close();
            widUrl->deleteLater();
        });

        connect(txtUrl, &QLineEdit::textChanged, widUrl, [&, btnOk, lblUrl](const QString &text) {
            bool valid = validateURL(text);
            if (valid) {
                lblUrl->setText("");
                btnOk->setEnabled(true);
            } else {
                lblUrl->setText("Not valid. If not added http:// add it.");
                lblUrl->setStyleSheet("color:red");
                btnOk->setEnabled(false);
            }
        });

        connect(btnOk, &QPushButton::clicked, widUrl, [&, btnCnl]() {
           btnCnl->click();
           ui->path->setText(txtUrl->text());
        });

        qDebug() << widUrl->exec();
    }
}

void sessionSaveDialog::on_addAppPath_clicked()
{
    // add an empty row .there add text like in first colum set app ,second give file ,folder or Url.
    int index = ui->sesList->rowCount();
    QString tempAppN = "<Enter an app>";
    QString tempPath = "<Select file, folder or url>";
    if (index) {
        QString currAppN = ui->sesList->item(index - 1, 0)->text();
        QString currPath = ui->sesList->item(index - 1, 1)->text();
        if (currAppN != tempAppN && currPath != tempPath) {
            ui->sesList->setRowCount(index + 1);
            ui->sesList->setItem(index, 0, new QTableWidgetItem(tempAppN));
            ui->sesList->setItem(index, 1, new QTableWidgetItem(tempPath));
        } else {
            qDebug() << "Warning : New item already inserted.";
            return;
        }
    } else {
        ui->sesList->setRowCount(index + 1);
        ui->sesList->setItem(index, 0, new QTableWidgetItem(tempAppN));
        ui->sesList->setItem(index, 1, new QTableWidgetItem(tempPath));
    }

    ui->sesList->setCurrentItem(ui->sesList->item(index, 0));
    ui->app->setFocus();
    ui->addAppPath->setVisible(false);
}

void sessionSaveDialog::on_removeAppPath_clicked()
{
    //delete the selected app and path , full row.
    if (ui->sesList->currentItem()) {
        int index = ui->sesList->currentRow();
        QStringList data = ui->sesList->item(index, 1)->data(Qt::UserRole).toString().split("\t\t\t");
        if (data.count() == 3) {
            QString appName = data.at(0);
            QString path    = data.at(1);
            QString key     = data.at(2); // DateTime

            QSettings session(CPrime::Variables::CC_CoreApps_SessionFilePath(), QSettings::IniFormat);
            session.beginGroup(ui->sessionName->text());
            session.beginGroup(appName);
            QString value = session.value(key).toString();
            qDebug() << "info to delete\n\tappName : " << appName << "\n\tpath : " << path << "\n\tkey : " << key << "\n\tvalue : " << value;
            if (value == path) {
                session.remove(key);
            }
            session.endGroup();
            session.endGroup();
            itemCount++;
        } else if (data.count() == 0){
            itemCount--;
        }

        ui->sesList->removeRow(ui->sesList->currentRow());

        emit enableOkSignal();
    }
}

void sessionSaveDialog::enableDone(const QString &str)
{
    Q_UNUSED(str);
    if (ui->app->text().count() && ui->path->text().count()) {
        if (enablePath(ui->path->text()) || validateURL(ui->path->text())) {
            ui->path->setStyleSheet("color:none;");
            ui->done->setVisible(true);
        } else {
            ui->path->setStyleSheet("color:red;");
        }
    } else {
        ui->done->setVisible(false);
        ui->path->setStyleSheet("color:gray;");
    }
}

bool sessionSaveDialog::enablePath(const QString &str)
{
    if (str.count()) {
        QFileInfo fi(str);

        if (!fi.exists()) {
            return false;
        } else {
            return true;
        }
    } else {        
        return false;
    }
}

void sessionSaveDialog::on_sesList_itemSelectionChanged()
{
    if (ui->sesList->currentItem()) {
        ui->pathEdit->setVisible(1);
        ui->removeAppPath->setVisible(1);

        int index = ui->sesList->currentIndex().row();
        QString dateTime = ui->sesList->item(index, 0)->data(Qt::UserRole).toString();
        if (!dateTime.count()) {
            ui->app->setText(nullptr);
            ui->path->setText(nullptr);

            ui->app->setPlaceholderText(ui->sesList->item(index, 0)->text());
            ui->path->setPlaceholderText(ui->sesList->item(index, 1)->text());
        } else {
            ui->app->setText(ui->sesList->item(index, 0)->text());
            ui->path->setText(ui->sesList->item(index, 1)->text());
        }

        emit enableOkSignal();
    } else {
        ui->pathEdit->setVisible(0);
        ui->removeAppPath->setVisible(0);
    }
}

void sessionSaveDialog::on_done_clicked()
{
    if (!ui->app->text().count()) {
        qDebug() << "Error : Still app name is empty.";
        return;
    }

    // If don't want path for app comment this section.
    if (!ui->path->text().count()) {
        qDebug() << "Error : Still path is empty.";
        return;
    }

    bool pathV = !QFileInfo(ui->path->text()).exists();
    bool urlV  = !validateURL(ui->path->text());
    if (pathV && urlV) {
        if (urlV) {
            qDebug() << "Error : URL invalid.";
            return;
        }
        qDebug() << "Error : Path invalid.";
        return;
    }
    // -----------------------------------------------

    int index = ui->sesList->currentIndex().row();
    QString currAppN = ui->sesList->item(index, 0)->text();
    QString currPath = ui->sesList->item(index, 1)->text();
    if (currAppN != ui->app->text() || currPath != ui->path->text()) {
        ui->sesList->item(index, 0)->setText(ui->app->text());
        ui->sesList->item(index, 0)->setIcon(CPrime::ThemeFunc::getAppIcon(ui->app->text()));
        ui->sesList->item(index, 0)->setData(Qt::UserRole, QDateTime::currentDateTime().toString("hh.mm.ss - dd.MM.yyyy"));
        ui->sesList->item(index, 1)->setText(ui->path->text());
        if ((currAppN != ui->app->text() && currPath != ui->path->text()) || ui->sesList->item(index, 0)->data(Qt::UserRole).toString().count()) {
            itemCount++;
        }
        emit enableOkSignal();
        ui->addAppPath->setVisible(true);
    } else {
        qDebug() << "Warning : Already added for current item.";
    }

    qDebug() << "ItemCount : " << itemCount;
}

bool sessionSaveDialog::validateURL(const QString &url)
{
    QRegExp urlRegex("^(http|https|ftp|ftps|bt)://[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(([0-9]{1,5})?/?.*)$");
    QRegExpValidator validator(urlRegex);
    int index = 0;

    QString temp = url;
    if (validator.validate(temp, index) == QValidator::Acceptable) {
        return true;
    }
    return false;
}

void sessionSaveDialog::enableOk()
{
    qDebug() << "ItemCount : " << itemCount;
    if (itemCount) {
        ui->ok->setEnabled(true);
    } else {
        ui->ok->setEnabled(false);
    }
}
