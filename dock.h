/*
    *
    * This file is a part of CoreBox.
    * A Desktop for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#ifndef CoreDock_H
#define CoreDock_H

#include <QToolButton>
#include <QMouseEvent>
#include <QShortcut>
#include <QTimer>
#include <QEvent>
#include <QHBoxLayout>


#include <cprime/settingsmanage.h>

class CoreDock : public QWidget
{
Q_OBJECT

public:
	CoreDock();

private:
	QPoint mousePos;
	bool isMouseDown = false ;
	bool isCStuffShown = false;

	SettingsManage *sm = SettingsManage::instance();

	int position = 0;

	bool dockHidden = false;

	/* 30px CureStuff button */
	int actualSize = 30;

	QHBoxLayout *dockLyt;
	QTimer *autoHideTimer;

	void loadButton();
	void loadSystemTray();

	void loadCoreAction();

	void setup();
	void loadSettings();
	void shotcuts();

private slots:
	void showMenu();
	void resizeDock( int );

	void unhideDock();
	void autoHideDock();

protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);

	void enterEvent( QEvent *event );
	void leaveEvent( QEvent *event );
};

#endif // CoreDock_H
