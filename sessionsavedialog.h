/*
    *
    * This file is a part of CoreBox.
    * A Desktop for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QDialog>
#include <QTableWidgetItem>
#include <QFileDialog>


namespace Ui {
    class sessionSaveDialog;
}

class sessionSaveDialog : public QDialog {

    Q_OBJECT

public:
    explicit sessionSaveDialog(bool editAction, QString sessionName, /*QString date = nullptr,*/ QWidget *parent = nullptr);
    ~sessionSaveDialog();

    QStringList getBookNames(QString sectionName);

signals:
    void enableOkSignal();

private slots:
    void on_ok_clicked();
    void on_sessionName_textChanged(const QString &arg1);
    void on_changeApp_clicked();
    void on_changePath_clicked();
    void on_addAppPath_clicked();
    void on_removeAppPath_clicked();
    void on_sesList_itemSelectionChanged();
    void on_done_clicked();

private:
    Ui::sessionSaveDialog *ui;
    // Contains Session Name
    QStringList nameList;
    // Action List
    // 0 - Add Session
    // 1 - Edit Session
    bool m_action;
    int itemCount;

    bool validateURL(const QString &url);
    void enableOk();
    void enableDone(const QString &str);
    bool enablePath(const QString &str);

};

