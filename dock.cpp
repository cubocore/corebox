/*
    *
    * This file is a part of CoreBox.
    * A Desktop for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include "corebox_dock.h"
#include "dock.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QScreen>
#include <QProcess>

#include <cprime/appopenfunc.h>
#include <cprime/cprime.h>

CoreDock::CoreDock() : QWidget()
{
	dockLyt = new QHBoxLayout();
	dockLyt->setContentsMargins( QMargins() );
	dockLyt->setSpacing( 0 );

	loadButton();
	loadSystemTray();

	setLayout( dockLyt );

	loadCoreAction();

	setFixedHeight( 30 );
	setMinimumWidth( 30 );

    loadSettings();
    setup();
    shotcuts();

    setAttribute( Qt::WA_ShowWithoutActivating );
    setAttribute( Qt::WA_X11NetWmWindowTypeDock );

    setWindowFlags( Qt::ToolTip | Qt::FramelessWindowHint );

    autoHideTimer = new QTimer( this );
    autoHideTimer->setSingleShot( true );

    connect( autoHideTimer, SIGNAL( timeout() ), this, SLOT( autoHideDock() ) );
}

void CoreDock::loadButton() {

	cstuff = new corestuff();

	QToolButton *dockButton = new QToolButton();
	dockButton->setIcon( QIcon( ":/icons/corebox.svg" ) );
	dockButton->setFixedSize( QSize( 30, 30 ) );
	dockButton->setIconSize( QSize( 24, 24 ) );

	connect( dockButton, SIGNAL( clicked() ), this, SLOT( showMenu() ) );

	dockLyt->addWidget( dockButton );
};

void CoreDock::loadCoreAction() {

	QStringList paths = QString::fromLocal8Bit( qgetenv( "PATH" ) ).split( ":" );
	Q_FOREACH( QString path, paths ) {
		if ( QFile( path + "/coreaction" ).exists() ) {
			QProcess::startDetached( path + "/coreaction" );
			return;
		}
	}

	CPrime::InfoFunc::messageEngine( "CoreAction not found! Please install CoreAction for better functionality", CPrime::MessageType::Info, this );
}

void CoreDock::loadSystemTray() {

	LXQtTray *tray = new LXQtTray( this );
	connect( tray, SIGNAL( traySizeChanged( int ) ), this, SLOT( resizeDock( int ) ) );

	tray->setFixedHeight( 30 );
	tray->setIconSize( QSize( 24, 24 ) );

	dockLyt->addWidget( tray );

	tray->resize( 0, 0 );
	tray->updateGeometry();
};

void CoreDock::showMenu()
{
    if ( not isCStuffShown ) {

        cstuff->show();
        isCStuffShown = true;

    } else {

        cstuff->hide();
        isCStuffShown = false;
    }
}

void CoreDock::resizeDock( int value )
{
	if ( not dockHidden ) {
		/* 5 px buffer */
		setFixedWidth( 30 + value + 5 );
	}

	actualSize = 30 + value + 5;
}

/**
 * @brief Loads application settings
 */
void CoreDock::loadSettings()
{
    // Set position from settings
    QString posStr = sm->getPosition();
    if (posStr == "Bottom") {
        position = 1;

    } else if (posStr == "Left") {
        position = 2;

    } else if (posStr == "Right") {
        position = 3;

    } else if (posStr == "Top") {
        position = 4;

    }
}

void CoreDock::mousePressEvent(QMouseEvent *event)
{
    if (underMouse()) {
        isMouseDown = true;
        mousePos = event->pos();
    }

    // if (event->type() == QEvent::MouseButtonPress){
        // showMenu();
    // }
    // if (event->type() == QEvent::MouseButtonDblClick){

        // showCoreAction();
    // }

    QWidget::mousePressEvent(event);
}

void CoreDock::mouseMoveEvent(QMouseEvent *)
{
//    if(isMouseDown==true){
//        int mousePointx = (event->globalX() - mousePos.x());
//        int mousePointy = (event->globalY() - mousePos.y());
//        move(mousePointx, mousePointy);
//    }
}

void CoreDock::enterEvent(QEvent *)
{
	if ( not dockHidden )
		return;

	if ( autoHideTimer->isActive() )
		autoHideTimer->stop();

	/* Show unhide the dock after 200 ms */
	QTimer::singleShot( 200, this, SLOT( unhideDock() ) );
}

void CoreDock::leaveEvent(QEvent *)
{
	autoHideTimer->start( 2000 );
}

void CoreDock::shotcuts()
{
    QShortcut *shortcut;
    shortcut = new QShortcut( QKeySequence( Qt::CTRL + Qt::Key_A ), this );
    connect( shortcut, &QShortcut::activated, this, &CoreDock::showMenu );
}

void CoreDock::setup()
{

    QRect src = qApp->screens()[0]->geometry();
    int Width = src.width();
    int Height = src.height();

    switch(position)
    {
    case 1: // Bottom Left
        move( 0, Height - height() );
        break;

    case 2: // Top Left
        move( 0, 0 );
        break;

    case 3: // Bottom Right
        move( Width - width(), Height - height() );
        break;

    case 4: // Top Right
        move( Width - width(), 0 );
        break;

    default:// Left is default
        move( 0, Height - height() );
        break;
    }
}

void CoreDock::autoHideDock()
{
	if ( underMouse() )
		return;

	/* Set the width to be 3px */
	setFixedWidth( 3 );
	dockHidden = true;
}

void CoreDock::unhideDock()
{
	/* Set the width to be 2px */
	setFixedWidth( actualSize );
	dockHidden = false;
}
