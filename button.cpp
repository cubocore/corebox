/*
    *
    * This file is a part of CoreBox.
    * A Desktop for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include "button.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QScreen>
#include <QProcess>
#include <QDebug>


DockButton::DockButton()
{
    isDockHide = true;

    resize(50,50);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);

    cstuff = new corebox();

    connect(this, &DockButton::clicked, this, &DockButton::onClick);
}

DockButton::~DockButton()
{
    cstuff->deleteLater();
}

void DockButton::onClick()
{
    if (isDockHide) {
        cstuff->show();
        isDockHide = false;
    } else {
        settingsManage::initialize()->setValue("CoreBox", "WidgetView-Width", cstuff->widgetViewWidth);
        cstuff->hide();
        isDockHide = true;
    }
}

void DockButton::mousePressEvent(QMouseEvent *event)
{
    if (underMouse()) {
        mousePos = event->pos();
    }

    QPushButton::mousePressEvent(event);
}

void DockButton::mouseMoveEvent(QMouseEvent *event) {
    int mousePointx = (event->globalX() - mousePos.x());
    int mousePointy = (event->globalY() - mousePos.y());
    move(mousePointx, mousePointy);
}
