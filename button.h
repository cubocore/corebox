/*
    *
    * This file is a part of CoreBox.
    * A Desktop for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#ifndef BUTTON_H
#define BUTTON_H

#include <QObject>
#include <QToolButton>
#include <QMouseEvent>
#include <QTimer>
#include <QEvent>
#include <QHBoxLayout>
#include <QPushButton>
#include <QMouseEvent>

#include "corebox.h"



class DockButton : public QPushButton
{

public:
    DockButton();
    ~DockButton();
    bool isDockHide;

private:
    QPoint mousePos;
    void onClick();

    corebox *cstuff = nullptr;

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

};


#endif // BUTTON_H
